import java.text.DecimalFormat;

public class LoadRecord  {
    String HEADER;
    String ID;
    String PDES;
    String LDES;
    String PLUS;
    String LOAD;
    String TYPE;
    String LPORT;
    String DPORT;
    String EXTRA;
    double LENX;
    double L;
    double LENY;
    double B;
    double HS;
    double LENZ;
    double UNITW;
    String IMO;
    String UNNO;
    String EMS;
    String MFAG;
    String IMDG;
    String PGR;
    String MP;
    int W;
    String LQ;
    int NR;
    int NL;
    int PCS;
    String STATUS;
    String LFCODE;
    double W1;
    double W2;
    double STF;
    double MASSL;
    String MODE;
    double MASS;
    double MASSH;
    String CAS;
    int HDECK;
    String ACODE;

    public String getHEADER() {
        return HEADER;
    }

    public void setHEADER(String HEADER) {
        this.HEADER = HEADER;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getPDES() {
        return PDES;
    }

    public void setPDES(String PDES) {
        this.PDES = PDES;
    }

    public String getLDES() {
        return LDES;
    }

    public void setLDES(String LDES) {
        this.LDES = LDES;
    }

    public String getPLUS() {
        return PLUS;
    }

    public void setPLUS(String PLUS) {
        this.PLUS = " ";
    }



    public String getLOAD() {
        return LOAD;
    }

    public void setLOAD(String LOAD) {
        this.LOAD = LOAD;
    }

    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE() {
        this.TYPE = "RORO";
    }

    public String getLPORT() {
        return LPORT;
    }

    public void setLPORT(String LPORT) {
        this.LPORT = LPORT;
    }

    public String getDPORT() {
        return DPORT;
    }

    public void setDPORT(String DPORT) {
        this.DPORT = DPORT;
    }

    public String getEXTRA() {
        return EXTRA;
    }

    public void setEXTRA(String EXTRA) {
        this.EXTRA = EXTRA;
    }

    public double getLENX() {
        return LENX;
    }

    public void setLENX(double LENX) {
        this.LENX = LENX;
    }

    public double getL() {
        return L;
    }

    public void setL(double l) {
        L = l;
    }

    public double getLENY() {
        return LENY;
    }

    public void setLENY(double LENY) {
        this.LENY = LENY;
    }

    public double getB() {
        return B;
    }

    public void setB(double b) {
        B = b;
    }

    public double getHS() {
        return HS;
    }

    public void setHS(double HS) {
        this.HS = HS;
    }

    public double getLENZ() {
        return LENZ;
    }

    public void setLENZ(double LENZ) {
        this.LENZ = LENZ;
    }

    public double getUNITW() {
        return UNITW;
    }

    public void setUNITW(double UNITW) {
        this.UNITW = UNITW;
    }

    public String getIMO() {
        return IMO;
    }

    public void setIMO(String IMO) {
        this.IMO = IMO;
    }

    public String getUNNO() {
        return UNNO;
    }

    public void setUNNO(String UNNO) {
        this.UNNO = UNNO;
    }

    public String getEMS() {
        return EMS;
    }

    public void setEMS(String EMS) {
        this.EMS = EMS;
    }

    public String getMFAG() {
        return MFAG;
    }

    public void setMFAG(String MFAG) {
        this.MFAG = MFAG;
    }

    public String getIMDG() {
        return IMDG;
    }

    public void setIMDG(String IMDG) {
        this.IMDG = IMDG;
    }

    public String getPGR() {
        return PGR;
    }

    public void setPGR(String PGR) {
        this.PGR = PGR;
    }

    public String getMP() {
        return MP;
    }

    public void setMP(String MP) {
        this.MP = MP;
    }

    public int getW() {
        return W;
    }

    public void setW(int w) {
        W = w;
    }

    public String getLQ() {
        return LQ;
    }

    public void setLQ(String LQ) {
        this.LQ = LQ;
    }

    public int getNR() {
        return NR;
    }

    public void setNR(int NR) {
        this.NR = NR;
    }

    public int getNL() {
        return NL;
    }

    public void setNL(int NL) {
        this.NL = NL;
    }

    public int getPCS() {
        return PCS;
    }

    public void setPCS(int PCS) {
        this.PCS = PCS;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getLFCODE() {
        return LFCODE;
    }

    public void setLFCODE(String LFCODE) {
        this.LFCODE = LFCODE;
    }

    public double getW1() {
        return W1;
    }

    public void setW1(double w1) {
        W1 = w1;
    }

    public double getW2() {
        return W2;
    }

    public void setW2(double w2) {
        W2 = w2;
    }

    public double getSTF() {
        return STF;
    }

    public void setSTF(double STF) {
        this.STF = STF;
    }

    public double getMASSL() {
        return MASSL;
    }

    public void setMASSL(double MASSL) {
        this.MASSL = MASSL;
    }

    public String getMODE() {
        return MODE;
    }

    public void setMODE(String MODE) {
        this.MODE = MODE;
    }

    public double getMASS() {
        return MASS;
    }

    public void setMASS(double MASS) {
        this.MASS = MASS;
    }

    public double getMASSH() {
        return MASSH;
    }

    public void setMASSH(double MASSH) {
        this.MASSH = MASSH;
    }

    public String getCAS() {
        return CAS;
    }

    public void setCAS(String CAS) {
        this.CAS = CAS;
    }

    public int getHDECK() {
        return HDECK;
    }

    public void setHDECK(int HDECK) {
        this.HDECK = HDECK;
    }

    public String getACODE() {
        return ACODE;
    }

    public void setACODE(String ACODE) {
        this.ACODE = ACODE;
    }

    @Override
    public String toString() {

        DecimalFormat df = new DecimalFormat("#.#");
        return '\n' +  ID +
                "," + PDES +
                "," + LDES +
                "," + PLUS +
                "," + LOAD +
                "," + TYPE +
                "," + LPORT +
                "," + DPORT +
                "," + EXTRA +
                "," + df.format(LENX) +
                "," + df.format(L) +
                "," + df.format(LENY) +
                "," + df.format(B) +
                "," + HS +
                "," + df.format(LENZ) +
                "," + df.format(UNITW) +
                "," + IMO +
                "," + UNNO +
                "," + EMS +
                "," + MFAG +
                "," + IMDG +
                "," + PGR +
                "," + MP +
                "," + W +
                "," + LQ +
                "," + NR +
                "," + NL +
                "," + PCS +
                "," + STATUS +
                "," + LFCODE +
                "," + W1 +
                "," + W2 +
                "," + STF +
                "," + df.format(MASSL) +
                "," + MODE +
                "," + df.format(MASS) +
                "," + df.format(MASSH) +
                "," + CAS +
                "," + HDECK +
                "," + ACODE;
    }
}
