
import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.charset.MalformedInputException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class InputData implements ActionListener {
    private JFileChooser fileopen = new JFileChooser();
    private final String[][] FILTER = {{"txt", "file TXT (*.txt)"}};
    private int total;
    private static int totalCars;
    InputData() {
    }



    @Override
    public void actionPerformed(ActionEvent e) {
        {
            int ret = fileopen.showDialog(null, "Open file");

            if (ret == JFileChooser.APPROVE_OPTION) {
                File file = fileopen.getSelectedFile();
                try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                    MainFrame.addData("\n Reading file" + file.getAbsolutePath()+"\n");

                    String s;
                    while ((s = br.readLine()) != null) {

                        String[] words = s.split(" ");
                        MainFrame.addData(s);
                        MainFrame.addData("\n");

                        //vehicleList.put(words[0], words[1]);

                        switch(words[0])
                        {
                            case "MC": {VehicleQueue.setNumMC(Integer.parseInt(words[1])); break;}
                            case "CAR": {VehicleQueue.setNumCAR(Integer.parseInt(words[1])); break;}
                            case "SHP_CAR": {VehicleQueue.setNumSHP_CAR(Integer.parseInt(words[1])); break;}
                            case "VAN": {VehicleQueue.setNumVAN(Integer.parseInt(words[1])); break;}
                            case "VAN_H": {VehicleQueue.setNumVAN_H(Integer.parseInt(words[1])); break;}
                            case "L_L": {VehicleQueue.setNumL_L(Integer.parseInt(words[1])); break;}
                            case "L_MH": {VehicleQueue.setNumL_MH(Integer.parseInt(words[1])); break;}
                            case "BUS": {VehicleQueue.setNumBUS(Integer.parseInt(words[1])); break;}
                            case "LOR": {VehicleQueue.setNumLOR(Integer.parseInt(words[1])); break;}
                            case "TT_25": {VehicleQueue.setNumTT_25(Integer.parseInt(words[1])); break;}
                            case "TT_39": {VehicleQueue.setNumTT_39(Integer.parseInt(words[1])); break;}
                            case "MCH": {VehicleQueue.setNumMCH(Integer.parseInt(words[1])); break;}
                            case "TRL": {VehicleQueue.setNumTRL(Integer.parseInt(words[1])); break;}
                            case "TLNK": {VehicleQueue.setNumTLNK(Integer.parseInt(words[1])); break;}
                        }

                        total += Integer.parseInt(words[1]);

                    }

                    MainFrame.addData("\n Total: " + total);
                    totalCars=total;
                    if (total>0)  {MainFrame.activateStartProcessButton();MainFrame.deactivateFileOpenButton();}
                } catch (IOException ex) {
                    MainFrame.addData(ex.getMessage());
                }
            } else if (ret == JFileChooser.CANCEL_OPTION) {
                MainFrame.addData("\n Cancelled file open");
            }
        }
    }
    // end of actionListener

    public static void setTotalCars(int total) {
        totalCars = total;
    }

    public static int getTotalCars() {
        return totalCars;
    }
}
