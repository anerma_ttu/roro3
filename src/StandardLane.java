import java.util.ArrayList;
import java.util.List;

public class StandardLane implements Lane {
    ArrayList<Vehicle> laneList;
    private String laneName;
    private String laneDeck;// 1 - from MaxX to minX , 0 - from Xmin to Xmax
    private int deckNum;
    private double lanePriority;
    private int laneDefaultPriority;
    private double laneCoefficient;
    private double laneCoefficientReversed;
    private float laneMass;
    private float centerX;
    private float centerY;
    private float centerZ;
    private float minX;
    private float maxX;
    private float setPoint;
    private float availableLength;
    public List<LaneLocation> laneLocation;
    public List<VehicleType> allowedVehicleList;
    public List <VehicleType> notAllowedVehicleList;

    //constructors

    public StandardLane(String name, String deck, int deckNum, float Y, float Z, float minX, float maxX) {
        this.laneList = new ArrayList<>();
        this.laneName = name;
        this.laneDeck = deck;
        this.deckNum = deckNum;
        this.centerY = Y;
        this.centerZ = Z;
        this.minX = minX;
        this.maxX = maxX;
        this.calculateInitialSetPoint();
        this.calculateAvailableLength();
    }

    public StandardLane(String name, String deck, int decknum, float Y, float Z, float minX, float maxX, float priority, List<LaneLocation> laneLocation, List<VehicleType> allowedVehiclesList) {
        this.laneList = new ArrayList<>();
        this.laneName = name;
        this.laneDeck = deck;
        this.deckNum = decknum;
        this.centerY = Y;
        this.centerZ = Z;
        this.minX = minX;
        this.maxX = maxX;
        this.lanePriority = priority;
        this.laneLocation=laneLocation;
        this.allowedVehicleList = allowedVehiclesList;
        this.calculateInitialSetPoint();
        this.calculateAvailableLength();
    }

    public StandardLane(String name, String deck, int decknum, float Y, float Z, float minX, float maxX, int defaultPriority, double priority, double coefficient, double reverseCoefficient, List<LaneLocation> laneLocation, List<VehicleType> allowedVehiclesList, List<VehicleType> notAllowedVehiclesList) {
        this.laneList = new ArrayList<>();
        this.laneName = name;
        this.laneDeck = deck;
        this.deckNum = decknum;
        this.centerY = Y;
        this.centerZ = Z;
        this.minX = minX;
        this.maxX = maxX;
        this.laneDefaultPriority = defaultPriority;
        this.lanePriority = priority;
        this.laneCoefficient= coefficient;
        this.laneCoefficientReversed = reverseCoefficient;
        this.laneLocation = laneLocation;
        this.allowedVehicleList = allowedVehiclesList;
        this.notAllowedVehicleList = notAllowedVehiclesList;
        this.setCenterX(minX,maxX);
        this.calculateInitialSetPoint();
        this.calculateAvailableLength();
    }

    // get functions

    public List<Vehicle> getVehicleList () {
        return laneList;
    }




    public String getName() {
        return laneName;
    }

    @Override
    public String getDeck() {
        return laneDeck;
    }

    public int getDeckNum() { return deckNum; }

    @Override
    public float getCenterX() {
        return centerX;
    }

    @Override
    public float getCenterY() {
        return centerY;
    }

    @Override
    public float getCenterZ() { return centerZ; }

    @Override
    public float getMinX() {
        return minX;
    }

    @Override
    public float getMaxX() { return maxX; }

    public double getPriority() { return lanePriority; }

    public int getDefaultPriority() { return laneDefaultPriority; }

    public float getAvailableLength() {
        return availableLength;
    }

    public float getLaneMass () { return this.laneMass; }

    public double getLaneCoefficient() { return laneCoefficient; }

    public double getDeckCoefficient() { return laneCoefficient; }

    public double getLaneCoefficientReversed() { return laneCoefficientReversed; }

    @Override
    public Lane getBestLane() { return null; }

    @Override
    public String toString() {
        return "\n Current lane: " +
                "\nlaneName='" + laneName + '\'' +
                "\nlaneDeck='" + laneDeck + '\'' +
                "\ndeckNum=" + deckNum +
                "\nlanePriority=" + lanePriority +
                "\nlaneMass=" + laneMass +
                "\ncenterY=" + centerY +
                "\ncenterZ=" + centerZ +
                "\nminX=" + minX +
                "\nmaxX=" + maxX +
                // current selection deleted
                "\nallowedVehicleList=" + allowedVehicleList +
                "\nnotAllowedVehicleList=" + notAllowedVehicleList +
                '}';
    }

    public void getLoadedVehiclesOnLane () {
        for (Vehicle vehicle : laneList) {
            vehicle.getVehicleData();
        }
    }

    public static double round3( double value ) {
        int iValue = ( int ) ( value * 1000 );
        double dValue = value * 1000;
        if ( dValue - iValue >= 0.5 ) {
            iValue += 1;
        }
        dValue = ( double ) iValue;
        return dValue / 1000;
    }

    public int getVehicleListSize ()
    {
        return getVehicleList().size();
    }

    // set functions

    public void setCenterX(float minX, float maxX) {
         centerX = minX + ((maxX-minX)/2);
    }

    public void setNewSetPoint( Vehicle vehicle)
    {
        if (Ferry.getDirection())
            setPoint = (float)round3(setPoint - vehicle.getVehicleLength());
        else
            setPoint = (float)round3(setPoint + vehicle.getVehicleLength());

    }

    private void setAvailableLength()
    {
        if (Ferry.getDirection())
            availableLength=(float)round3(setPoint-minX);
        else
            availableLength=(float)round3(maxX-setPoint);
    }

    public void setLaneCoefficient(double laneCoefficient) {
        this.laneCoefficient = laneCoefficient;
    }

    public void setLaneCoefficientReversed(double laneCoefficientReversed) {
        this.laneCoefficientReversed = laneCoefficientReversed;
    }


    // calculation function


    public void calculateAvailableLength() {
        if (Ferry.getDirection())
            this.availableLength = (float)round3(setPoint - minX);
        else
            this.availableLength = (float)round3 (maxX-setPoint);
    }

    public void calculateInitialSetPoint()
    {
        if (Ferry.getDirection())
            setPoint = maxX;
        else setPoint=minX;
    }

    @Override
    public void setPriority(double priority) {
    lanePriority=priority;
    }

    @Override
    public void sortLanes() {

    }


    // adding and removing functions

    public void addLoadedVehicleWeightToLaneMass (Vehicle vehicle) {  laneMass+=vehicle.getVehicleMass(); }

    @Override
    public int addVehicleToLane(Vehicle vehicle, boolean direction) {
        if (vehicle != null)
            try {
                LoadedVehicle tempVehicle = new LoadedVehicle((StandardVehicle) vehicle, this.laneName, this.laneDeck);
                tempVehicle.setMaxX(this.setPoint, vehicle.getVehicleLength());
                tempVehicle.setMinX(this.setPoint, vehicle.getVehicleLength());
                laneList.add(tempVehicle);
                this.addLoadedVehicleWeightToLaneMass(tempVehicle);
                this.setNewSetPoint(vehicle);
                // to do decrease length
                this.setAvailableLength();
                return 1;
            } catch (Exception ex) {
                ex.getStackTrace();
            }
        return 0;
    }

    @Override
    public int removeVehicleFromLane() {
        return 0;
    }



    // printing functions
    public void printLaneMass (Lane thisLane)
    {
        MainFrame.addData("\nLane" +thisLane.getName() + " load is " + laneMass + "tons");
    }

    // list returning
    @Override
    public List<VehicleType> getAllowedVehicleTypes() {
        return allowedVehicleList;
    }

    @Override
    public List<VehicleType> getNotAllowedVehicleTypes() {
        return notAllowedVehicleList;
    }


    public void exportToTable(List<Vehicle> listVehicle, Lane lane, List<Record> outputTable)
    {
        for (Vehicle currentVehicle: listVehicle)
        {
            Record tempRecord = new Record();
            tempRecord.setHEADER("\nTAB*OB_ROROLOAD\n" +
                    "LOAD,ID,LANE,PART,LPORT,DPORT,EXTRA,LDES,L,B,HS,H,LENX,LENY,LENZ,XMIN,X1,XMAX,X2,STATI,MASS,XM,YM,ZM,STATUS,YMIN,YMAX,ZMIN,ZMAX,IMO,UNNO,EMS,PGR,MFAG,IMDG,MP,LQ,W,W1,RNR,W2,W2MAX,AREA,VOL,STF,LFCODE,OPTION,NR,N1,UNITW,LAYER,HMAX,HDECK,WD,ERROR");
            tempRecord.setLOAD(currentVehicle.getVehicleType().name());
            tempRecord.setID(currentVehicle.getVehicleType().name());
            tempRecord.setLANE(currentVehicle.getLaneString());
            tempRecord.setPART(currentVehicle.getDeckString());
            tempRecord.setLPORT("");
            tempRecord.setDPORT("");
            tempRecord.setEXTRA(currentVehicle.getRegistrationNumber());
            tempRecord.setLDES(currentVehicle.getVehicleType().getVehicleDescription());
            tempRecord.setL((double)currentVehicle.getVehicleLength());
            tempRecord.setB((double)currentVehicle.getVehicleWidth());
            tempRecord.setHS(0);
            tempRecord.setH((double)currentVehicle.getVehicleHeight());
            tempRecord.setLENX(0);
            tempRecord.setLENY(0);
            tempRecord.setLENZ((double)currentVehicle.getVehicleHeight());
            tempRecord.setXMIN((double)currentVehicle.getxMin());
            tempRecord.setX1((double)currentVehicle.getxMin());
            tempRecord.setXMAX((double)currentVehicle.getxMax());
            tempRecord.setX2((double)currentVehicle.getxMax());
            tempRecord.setMASS((double)(currentVehicle.getVehicleMass()));
            tempRecord.setSTATI(1);
            tempRecord.setXM ( currentVehicle.getxMax() - (currentVehicle.getVehicleLength()*0.5));
            tempRecord.setYM (lane.getCenterY());

            tempRecord.setSTATUS("M");
            tempRecord.setZM (lane.getCenterZ());

            tempRecord.setYMIN(lane.getCenterY() - (currentVehicle.getVehicleWidth()*0.5));
            tempRecord.setYMAX(lane.getCenterY() + (currentVehicle.getVehicleWidth()*0.5));
            tempRecord.setZMIN(lane.getCenterZ());
            tempRecord.setZMAX(lane.getCenterZ()+currentVehicle.getVehicleHeight());
            tempRecord.setIMO("");
            tempRecord.setUNNO("");
            tempRecord.setEMS("");
            tempRecord.setPGR("");
            tempRecord.setMFAG("");
            tempRecord.setIMDG("");
            tempRecord.setMP("");
            tempRecord.setLQ("");
            tempRecord.setW(0);
            tempRecord.setW1(0);
            tempRecord.setRNR(0);
            tempRecord.setW2(0);
            tempRecord.setW2MAX(0);
            tempRecord.setAREA(currentVehicle.getVehicleLength()*currentVehicle.getVehicleWidth());
            tempRecord.setVOL(currentVehicle.getVehicleLength()*currentVehicle.getVehicleWidth()*currentVehicle.getVehicleHeight());
            tempRecord.setSTF(0);
            tempRecord.setLFCODE(currentVehicle.getVehicleType().getVehicleColour());
            tempRecord.setOPTION(" ");
            tempRecord.setNR(0);
            tempRecord.setN1(1);
            tempRecord.setUNITW(currentVehicle.getVehicleMass());
            tempRecord.setLAYER(0);
            tempRecord.setHMAX(0);
            tempRecord.setHDECK(0);
            tempRecord.setWD(0);
            tempRecord.setERROR(" ");

            outputTable.add(tempRecord);
        }
    }

    @Override
    public void calculatePriority() {

    }
    
    public void moveVehicles(boolean direction) {

    }


}



