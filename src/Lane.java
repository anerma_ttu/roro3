import java.util.List;

public interface Lane {

        // adding and removing

        int addVehicleToLane ( Vehicle vehicle, boolean direction) throws Exception;

        int removeVehicleFromLane();

        // sort and calculation
        void sortLanes ();

        void calculateAvailableLength ();

        void calculateInitialSetPoint ();


        // set functions

        void setPriority(double priority);

        // get functions

        String getName();

        String getDeck();

        float getCenterX();

        float getCenterZ();

        float getCenterY();

        void setCenterX (float minX, float maxX);

       // void setMinX();

      //  void setMaxX();

        float getMinX();


        float getMaxX();

        Lane getBestLane ();

        float getAvailableLength ();

        float getLaneMass ();

        double getPriority();

        int getDefaultPriority();

        double getLaneCoefficient();

        double getLaneCoefficientReversed();

        void getLoadedVehiclesOnLane();

        // printing data

        void printLaneMass(Lane lane);

        // returning List

        List<VehicleType> getAllowedVehicleTypes();

        List<VehicleType> getNotAllowedVehicleTypes();

        List<Vehicle> getVehicleList();

        void exportToTable(List<Vehicle> vehicleList, Lane lane, List<Record> outputTable);

        void calculatePriority();


}
