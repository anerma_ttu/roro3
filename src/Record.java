import java.text.DecimalFormat;

public class Record {
    String HEADER;
    String REGNUM;
    String LOAD;
    String ID;
    String LANE;
    String PART;
    String LPORT;
    String DPORT;
    String EXTRA;
    String LDES;
    double L;
    double B;
    double HS;
    double H;
    double LENX;
    double LENY;
    double LENZ;
    double XMIN;
    double X1;
    double XMAX;
    double X2;
    int STATI;
    double MASS;
    double XM;
    double YM;
    double ZM;
    String STATUS;
    double YMIN;
    double YMAX;
    double ZMIN;
    double ZMAX;
    String IMO;
    String UNNO;
    String EMS;
    String PGR;
    String MFAG;
    String IMDG;
    String MP;
    String LQ;
    double W;
    double W1;
    double RNR;
    double W2;
    double W2MAX;
    double AREA;
    double VOL;
    double STF;
    String LFCODE;
    String OPTION;
    int NR;
    int N1;
    double UNITW;
    int LAYER;
    int HMAX;
    int HDECK;
    int WD;
    String ERROR;


    Record ()
    {
    }
    public String getHEADER() {
        return HEADER;
    }

    public void setHEADER(String HEADER) {
        this.HEADER = HEADER;
    }


    public String getREGNUM() {
        return REGNUM;
    }

    public void setREGNUM(String REGNUM) {
        this.REGNUM = REGNUM;
    }

    public String getLOAD() {
        return LOAD;
    }

    public void setLOAD(String LOAD) {
        this.LOAD = LOAD;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getLANE() {
        return LANE;
    }

    public void setLANE(String LANE) {
        this.LANE = LANE;
    }

    public String getPART() {
        return PART;
    }

    public void setPART(String PART) {
        this.PART = PART;
    }

    public String getLPORT() {
        return LPORT;
    }

    public void setLPORT(String LPORT) {
        this.LPORT = LPORT;
    }

    public String getDPORT() {
        return DPORT;
    }

    public void setDPORT(String DPORT) {
        this.DPORT = DPORT;
    }

    public String getEXTRA() {
        return EXTRA;
    }

    public void setEXTRA(String EXTRA) {
        this.EXTRA = EXTRA;
    }

    public String getLDES() {
        return LDES;
    }

    public void setLDES(String LDES) {
        this.LDES = LDES;
    }

    public double getL() {
        return L;
    }

    public void setL(double l) {
        L = l;
    }

    public double getB() {
        return B;
    }

    public void setB(double b) {
        B = b;
    }

    public double getHS() {
        return HS;
    }

    public void setHS(double HS) {
        this.HS = HS;
    }

    public double getH() {
        return H;
    }

    public void setH(double h) {
        H = h;
    }

    public double getLENX() {
        return LENX;
    }

    public void setLENX(double LENX) {
        this.LENX = LENX;
    }

    public double getLENY() {
        return LENY;
    }

    public void setLENY(double LENY) {
        this.LENY = LENY;
    }

    public double getLENZ() {
        return LENZ;
    }

    public void setLENZ(double LENZ) {
        this.LENZ = LENZ;
    }

    public double getXMIN() {
        return XMIN;
    }

    public void setXMIN(double XMIN) {
        this.XMIN = XMIN;
    }

    public double getX1() {
        return X1;
    }

    public void setX1(double x1) {
        X1 = x1;
    }

    public double getXMAX() {
        return XMAX;
    }

    public void setXMAX(double XMAX) {
        this.XMAX = XMAX;
    }

    public double getX2() {
        return X2;
    }

    public void setX2(double x2) {
        X2 = x2;
    }

    public int getSTATI() {
        return STATI;
    }

    public void setSTATI(int STATI) {
        this.STATI = STATI;
    }

    public double getMASS() {
        return MASS;
    }

    public void setMASS(double MASS) {
        this.MASS = MASS;
    }

    public double getXM() {
        return XM;
    }

    public void setXM(double XM) {
        this.XM = XM;
    }

    public double getYM() {
        return YM;
    }

    public void setYM(double YM) {
        this.YM = YM;
    }

    public double getZM() {
        return ZM;
    }

    public void setZM(double ZM) {
        this.ZM = ZM;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public double getYMIN() {
        return YMIN;
    }

    public void setYMIN(double YMIN) {
        this.YMIN = YMIN;
    }

    public double getYMAX() {
        return YMAX;
    }

    public void setYMAX(double YMAX) {
        this.YMAX = YMAX;
    }

    public double getZMIN() {
        return ZMIN;
    }

    public void setZMIN(double ZMIN) {
        this.ZMIN = ZMIN;
    }

    public double getZMAX() {
        return ZMAX;
    }

    public void setZMAX(double ZMAX) {
        this.ZMAX = ZMAX;
    }

    public String getIMO() {
        return IMO;
    }

    public void setIMO(String IMO) {
        this.IMO = IMO;
    }

    public String getUNNO() {
        return UNNO;
    }

    public void setUNNO(String UNNO) {
        this.UNNO = UNNO;
    }

    public String getEMS() {
        return EMS;
    }

    public void setEMS(String EMS) {
        this.EMS = EMS;
    }

    public String getPGR() {
        return PGR;
    }

    public void setPGR(String PGR) {
        this.PGR = PGR;
    }

    public String getMFAG() {
        return MFAG;
    }

    public void setMFAG(String MFAG) {
        this.MFAG = MFAG;
    }

    public String getIMDG() {
        return IMDG;
    }

    public void setIMDG(String IMDG) {
        this.IMDG = IMDG;
    }

    public String getMP() {
        return MP;
    }

    public void setMP(String MP) {
        this.MP = MP;
    }

    public String getLQ() {
        return LQ;
    }

    public void setLQ(String LQ) {
        this.LQ = LQ;
    }

    public double getW() {
        return W;
    }

    public void setW(double w) {
        W = w;
    }

    public double getW1() {
        return W1;
    }

    public void setW1(double w1) {
        W1 = w1;
    }

    public double getRNR() {
        return RNR;
    }

    public void setRNR(double RNR) {
        this.RNR = RNR;
    }

    public double getW2() {
        return W2;
    }

    public void setW2(double w2) {
        W2 = w2;
    }

    public double getW2MAX() {
        return W2MAX;
    }

    public void setW2MAX(double w2MAX) {
        W2MAX = w2MAX;
    }

    public double getAREA() {
        return AREA;
    }

    public void setAREA(double AREA) {
        this.AREA = AREA;
    }

    public double getVOL() {
        return VOL;
    }

    public void setVOL(double VOL) {
        this.VOL = VOL;
    }

    public double getSTF() {
        return STF;
    }

    public void setSTF(double STF) {
        this.STF = STF;
    }

    public String getLFCODE() {
        return LFCODE;
    }

    public void setLFCODE(String LFCODE) {
        this.LFCODE = LFCODE;
    }

    public String getOPTION() {
        return OPTION;
    }

    public void setOPTION(String OPTION) {
        this.OPTION = OPTION;
    }

    public double getNR() {
        return NR;
    }

    public void setNR(int NR) {
        this.NR = NR;
    }

    public int getN1() {
        return N1;
    }

    public void setN1(int n1) {
        N1 = n1;
    }

    public double getUNITW() {
        return UNITW;
    }

    public void setUNITW(double UNITW) {
        this.UNITW = UNITW;
    }

    public double getLAYER() {
        return LAYER;
    }

    public void setLAYER(int LAYER) {
        this.LAYER = LAYER;
    }

    public int getHMAX() {
        return HMAX;
    }

    public void setHMAX(int HMAX) {
        this.HMAX = HMAX;
    }

    public int getHDECK() {
        return HDECK;
    }

    public void setHDECK(int HDECK) {
        this.HDECK = HDECK;
    }

    public int getWD() {
        return WD;
    }

    public void setWD(int WD) {
        this.WD = WD;
    }

    public String getERROR() {
        return ERROR;
    }

    public void setERROR(String ERROR) {
        this.ERROR = ERROR;
    }
     /*
    @Override
    public String toString() {

        DecimalFormat df = new DecimalFormat("#.#");
        return  '\n' + LOAD +
             //   "\nREGNUM='" + REGNUM + '\'' +
                "," + ID  +
                "," + LANE +
                "," + PART +
                "," + LPORT +
                "," + DPORT +
                "," + EXTRA +
                "," + LDES +
                "," + df.format(L) +
                "," + df.format(B) +
                "," + HS +
                "," + df.format(H) +
                "," + df.format(LENX) +
                "," + df.format(LENY) +
                "," + df.format(LENZ) +
                "," + df.format(XMIN) +
                "," + df.format(X1) +
                "," + df.format(XMAX) +
                "," + df.format(X2) +
                "," + STATI +
                "," + df.format(MASS) +
                "," + df.format(XM) +
                "," + df.format(YM) +
                "," + df.format(ZM) +
                "," + STATUS +
                "," + df.format(YMIN) +
                "," + df.format(YMAX) +
                "," + df.format(ZMIN) +
                "," + df.format(ZMAX) +
                "," + IMO +
                "," + UNNO +
                "," + EMS +
                "," + PGR +
                "," + MFAG +
                "," + IMDG  +
                "," + MP +
                "," + LQ +
                "," + W +
                "," + W1 +
                "," + RNR +
                "," + W2 +
                "," + W2MAX +
                "," + df.format(AREA) +
                "," + df.format(VOL) +
                "," + STF +
                "," + LFCODE +
                "," + OPTION +
                "," + NR +
                "," + N1 +
                "," + df.format(UNITW) +
                "," + LAYER +
                "," + HMAX +
                "," + HDECK +
                "," + WD +
                "," + ERROR;
    }
    */

    @Override
    public String toString() {

        DecimalFormat df = new DecimalFormat("#.#");
        return    LOAD +
                //   "\nREGNUM='" + REGNUM + '\'' +
                " " + LANE +
                " " + df.format(XMIN) +'\n';}

}

